﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>使用Command对象添加数据</title>
    <style >
    body:{margin-top:0px}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style=" font-size :9pt">
           
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server">
                    </asp:GridView>
                </td>
            </tr>
             <tr>
                <td>
                    请输入类别名：<br />
                    <asp:TextBox ID="txtClass" runat="server" Width="78px"></asp:TextBox><asp:Button ID="btnAdd" runat="server"  Text="添加" OnClick="btnAdd_Click" /></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bind();
        }    
    }
    public SqlConnection GetConnection()
    {
        string myStr = ConfigurationManager.AppSettings["ConnectionString"].ToString();
        SqlConnection myConn = new SqlConnection(myStr);
        return myConn;
    }
    protected void bind()
    {
        SqlConnection myConn = GetConnection();
        myConn.Open();
        string sqlStr = "select * from tb_Class ";
        SqlDataAdapter myDa = new SqlDataAdapter(sqlStr, myConn);
        DataSet myDs = new DataSet();
        myDa.Fill(myDs);
       // ClassLibrary2.Class1 c1 = new ClassLibrary2.Class1();
       // DataSet ds = c1.GetData();
        GridView1.DataSource = myDs; 
        GridView1.DataBind();
        myDa.Dispose();
        myDs.Dispose();
        myConn.Close();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
      if (this.txtClass.Text != "")
      { 
          SqlConnection myConn = GetConnection();
          myConn.Open();
          string sqlStr = "insert into tb_Class(ClassName) values('"+ this.txtClass.Text.Trim() + "')";//插入数据
          SqlCommand myCmd = new SqlCommand(sqlStr, myConn);
          myCmd.ExecuteNonQuery();//执行插入操作
          myConn.Close();
         // ClassLibrary2.Class1 c1 = new ClassLibrary2.Class1();
         // c1.InsertData(this.txtClass.Text.Trim());
          this.bind();
      
      }
           
       else
            this.bind();
    }
}

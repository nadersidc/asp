﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.bind();
        }    
    }
    /// <summary>
    /// 返回创建的链接对象
    /// </summary>
    /// <returns></returns>
    public SqlConnection GetConnection()
    {
   //web.config中配置链接字符串
  //      <appSettings>
  //  <add key="ConnectionString" value="server=.;database=db_09;UId=sa;password=123456"/>
  //</appSettings>
        string myStr = ConfigurationManager.AppSettings["ConnectionString"].ToString();//获取链接字符串
        SqlConnection myConn = new SqlConnection(myStr);
        return myConn;
    }
    protected void bind()
    {
        SqlConnection myConn = GetConnection();//链接数据库
        myConn.Open();//打开数据库
        string sqlStr = "select * from tb_Student ";//定义查询字符串
        SqlDataAdapter myDa = new SqlDataAdapter(sqlStr, myConn);//sql数据适配器
        DataSet myDs = new DataSet();//创建dataset
        myDa.Fill(myDs);//SqlDataAdapter填充DataSet
        GridView1.DataSource = myDs;//数据表格控件数据源接收DataSet数据
        GridView1.DataBind();//执行绑定
        myDa.Dispose();//释放资源
        myDs.Dispose();//释放资源
        myConn.Close();//关闭链接
    }
   
    protected void btnSelect_Click(object sender, EventArgs e)
    {

        if (this.txtName.Text != "")
        {
            SqlConnection myConn = GetConnection();//建立数据库链接
            myConn.Open();//打开链接
            string sqlStr = "select * from tb_Student where Name=@Name";//定义查询字符串，@Name变量参数
            SqlCommand myCmd = new SqlCommand(sqlStr, myConn);//创建command，第一参数是查询的字符串，第二个参数是我们链接对象
            myCmd.Parameters.Add("@Name", SqlDbType.VarChar, 20).Value = this.txtName.Text.Trim();//给参数变量@Name，赋值
            SqlDataAdapter myDa = new SqlDataAdapter(myCmd);//适配器接收SqlCommand对象
            DataSet myDs = new DataSet();//创建DataSet
            myDa.Fill(myDs);//填充DATASET
            if (myDs.Tables[0].Rows.Count > 0)//判断查询出来的数据量
            {
                GridView1.DataSource = myDs;
                GridView1.DataBind();
            }
            else
            {

                Response.Write("<script>alert('没有相关记录')</script>");
            }
            myDa.Dispose();
            myDs.Dispose();
            myConn.Close(); 
        }
        else
            this.bind();
    }
}

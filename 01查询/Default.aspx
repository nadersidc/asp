﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>使用Command对象查询数据库中记录</title>
    <style >
    body:{margin-top:0px}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style=" font-size :9pt">
            <tr>
                <td>
                    请输入姓名：<asp:TextBox ID="txtName" runat="server" Width="78px"></asp:TextBox>
                    <asp:Button ID="btnSelect" runat="server" OnClick="btnSelect_Click" Text="查询" /></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
